-- Sprint 1
-- 1 Provide a query showing Customers (just their full names, customer ID and country) who are not in the US.

SELECT 
 CONCAT(FirstName, ' ', LastName), 
 CustomerID, 
 Country 
FROM Customer 
WHERE Country != 'USA'

-- 2 Provide a query only showing the Customers from Brazil.
SELECT * FROM Customer WHERE Country = 'Brazil';

-- 3 Provide a query showing the Invoices of customers who are from Brazil. The resultant table should show the customer's full name, Invoice ID, Date of the invoice and billing country.
SELECT 
  CONCAT (C.FirstName, ' ', C.LastName),
  I.InvoiceId, 
  I.InvoiceDate, 
  I.BillingCountry
FROM Customer as C 
INNER JOIN Invoice I on I.CustomerId = C.CustomerId
WHERE I.BillingCountry = 'Brazil';

-- 4 Provide a query showing only the Employees who are Sales Agents.
SELECT * FROM Employee WHERE Title = 'Sales Support Agent';

  -- 5 Provide a query showing a unique/distinct list of billing countries from the Invoice table.
SELECT DISTINCT BillingCountry FROM Invoice;


-- 6 Provide a query that shows the invoices associated with each sales agent. The resultant table should include the Sales Agent's full name.
SELECT
  CONCAT(E.FirstName, ' ', E.LastName) AS Name,
  I.InvoiceId
FROM Employee 
INNER JOIN Customer C on E.EmployeeId = C.SupportRepId
INNER JOIN Invoice I on C.CustomerId = I.CustomerId;

-- 7 Provide a query that shows the Invoice Total, Customer name, Country and Sale Agent name for all invoices and customers.
SELECT
  I.Total,
  CONCAT(C.FirstName, ' ', C.LastName) AS 'Customer Name',
  C.Country,
  CONCAT(E.FirstName, ' ', E.LastName) AS 'Employee Name'
FROM Invoice as I
INNER JOIN Customer C on I.CustomerId = C.CustomerId
INNER JOIN Employee E on E.EmployeeId = C.SupportRepId;

-- 8 How many Invoices were there in 2009 and 2011?

-- SELECT *   FROM Invoice Where InvoiceId = '2009' AND '2011';
SELECT COUNT(*) FROM Invoice WHERE strftime('%Y', InvoiceDate) BETWEEN '2009' AND '2011';


-- 9 What are the respective total sales for each of those years?
SELECT strftime('%Y', InvoiceDate) AS 'Year',
        SUM(Total) AS 'Total Sales'
  FROM Invoice 
WHERE strftime('%Y', InvoiceDate) IN('2009', '2011')
GROUP BY strftime('%Y', InvoiceDate)


-- 10 Looking at the InvoiceLine table, provide a query that COUNTs the number of line items for Invoice ID 37.
SELECT
COUNT(InvoiceLineId)
FROM InvoiceLine
WHERE InvoiceId = 37;

-- 11 Looking at the InvoiceLine table, provide a query that COUNTs the number of line items for each Invoice.
SELECT
COUNT(InvoiceLineId)
FROM InvoiceLine
GROUP BY (InvoiceId);

-- 12 Provide a query that includes the purchased track name with each invoice line item.
SELECT
  IL.InvoiceLineId,
  TR.Name as TrackName
FROM InvoiceLine as IL
INNER JOIN Track TR on IL.TrackId = TR.TrackId;


-- 13 Provide a query that includes the purchased track name AND artist name with each invoice line item.
SELECT 
  IL.InvoiceLineId, 
  TR.Name, 
  ART.Name 
FROM InvoiceLine IL 
INNER JOIN Track TR on TR.TrackId = IL.TrackId 
INNER JOIN Album ALB on ALB.AlbumId = TR.AlbumId 
INNER JOIN Artist ART on ART.ArtistId = ALB.ArtistId;

-- 14 Provide a query that shows the # of invoices per country.
SELECT
  BillingCountry,
  COUNT(InvoiceId)
FROM Invoice
GROUP BY BillingCountry;

-- 15 Provide a query that shows the total number of tracks in each playlist. The Playlist name should be include on the resultant table.
SELECT
  PL.Name AS 'Play_List_Name',
  COUNT(PLT.TrackId) AS 'Tracks'
FROM Playlist PL
INNER JOIN PlaylistTrack PLT ON PLT.PlaylistId = PL.PlaylistId
INNER JOIN Track TRK ON TRK.TrackId = PLT.TrackId
GROUP BY Play_List_Name;

-- 16 Provide a query that shows all the Tracks, but displays no IDs. The result should include the Album name, Media type and Genre.
SELECT
  TRK.Name as Track_Name,
  ALB.Title as Album_Name,
  MT.Name as Media_Type,
  GNR.Name as Genre
FROM Track TRK
INNER JOIN Album ALB on ALB.AlbumId = TRK.AlbumId
INNER JOIN Genre GNR on GNR.GenreId  = TRK.GenreId
INNER JOIN MediaType MT on MT.MediaTypeId = TRK.MediaTypeId;

-- 17 Provide a query that shows all Invoices but includes the # of invoice line items.
SELECT
  InvoiceId,
  COUNT (InvoiceId) as NumberInvoiceLineItem
FROM InvoiceLine
GROUP BY InvoiceId;

-- 18 Provide a query that shows total sales made by each sales agent.
SELECT 
  CONCAT(E.FirstName, ' ', E.LastName) AS Employee_Name,
  SUM(I.Total) AS Sales
FROM Customer C
INNER JOIN Employee E on E.EmployeeId = C.SupportRepId
INNER JOIN Invoice I on I.CustomerId = C.CustomerId
GROUP BY E.employeeId;

-- 19 Which sales agent made the most in sales in 2009?
SELECT 
  EmployeeName,
  MAX(Sales) AS Most_Sales
FROM 
  (SELECT
      CONCAT(E.FirstName, ' ', E.LastName) AS EmployeeName,
      SUM(I.Total) AS Sales
      FROM Customer C
      INNER JOIN Employee E ON E.EmployeeId = C.SupportRepId
      INNER JOIN Invoice I ON I.CustomerId = C.CustomerId
      WHERE strftime('%Y', I.InvoiceDate) IN ('2009')
      GROUP BY E.employeeId
  )

-- 20 Which sales agent made the most in sales over all?

SELECT 
  EmployeeName,
  MAX(Sales) AS TopSales
FROM 
  (SELECT
      E.FirstName, ' ', E.LastName AS EmployeeName,
      SUM(I.Total) AS Sales
      FROM Customer C
      INNER JOIN Employee E ON E.EmployeeId = C.SupportRepId
      INNER JOIN Invoice I ON I.CustomerId = C.CustomerId
      GROUP BY E.employeeId
  )
-- 21 Provide a query that shows the count of customers assigned to each sales agent.
SELECT 
  CONCAT(E.FirstName, ' ', E.LastName) AS Employee_Name,
  COUNT(C.SupportRepId) AS Customer_Count
FROM Customer C
INNER JOIN Employee E ON E.EmployeeId = C.SupportRepId
INNER JOIN Invoice I ON I.CustomerId = C.CustomerId
GROUP BY E.employeeId;


-- 22 Provide a query that shows the total sales per country.
  SELECT BillingCountry AS 'Country',    
      SUM(TOTAL), 2 AS 'Total Sales'
  FROM Invoice
GROUP BY BillingCountry;


-- 23 Which country's customers spent the most?
SELECT BillingCountry AS 'Country',    
      SUM(TOTAL) AS 'Total Sales'
  FROM Invoice
GROUP BY BillingCountry
ORDER BY 'Total Sales' DESC
LIMIT 1;

-- 24 Provide a query that shows the most purchased track of 2013.
SELECT 
  TrackName,
  MAX(Sold) AS 'Top1'
FROM 
  (SELECT
      TRK.Name AS 'TrackName',
      COUNT(IL.InvoiceLineId) AS Sold
      FROM Invoice I
      INNER JOIN InvoiceLine IL ON IL.InvoiceId= I.InvoiceId
      INNER JOIN Track TRK ON TRK.TrackId = IL.TrackId
      WHERE strftime('%Y', I.InvoiceDate) IN ('2013')
      GROUP BY TRK.TrackId
  )

-- 25 Provide a query that shows the top 5 most purchased tracks over all.
SELECT
  TRK.Name AS 'TrackName',
  COUNT(IL.InvoiceLineId) AS 'Sold'
FROM Invoice I 
INNER JOIN InvoiceLine IL ON IL.InvoiceId= I.InvoiceId
INNER JOIN Track TRK ON TRK.TrackId = IL.TrackId
GROUP BY TRK.TrackId
ORDER BY Sold DESC
LIMIT 5;

-- 26 Provide a query that shows the top 3 best selling artists.
SELECT
  AR.Name AS ArtistName,
  COUNT(IL.TrackId) AS 'TracksSold',
  SUM(IL.UnitPrice) AS 'Total'
FROM Album ALB
INNER JOIN Artist AR ON AR.ArtistId = ALB.ArtistId
INNER JOIN Track t ON t.AlbumId = ALB.AlbumId
INNER JOIN InvoiceLine IL ON IL.TrackId = t.TrackId
GROUP BY ArtistName
ORDER BY Total DESC
LIMIT 3;

-- 27 Provide a query that shows the most purchased Media Type.
SELECT
  MediaType,
  MAX(TotalSales) AS 'Top_Sales'
FROM
  (SELECT
      MT.Name AS 'MediaType',
      SUM(t.UnitPrice) AS 'TotalSales'
    FROM MediaType MT
    INNER JOIN Track t ON t.MediaTypeId = MT.MediaTypeId
    INNER JOIN InvoiceLine il ON il.TrackId = t.TrackId
    GROUP BY MediaType
  )
