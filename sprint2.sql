-- Sprint2

-- 1. Get all invoices where the UnitPrice on the InvoiceLine is greater than $0.99.

SELECT *
FROM Invoice I
JOIN InvoiceLine IL ON IL.InvoiceId = I.InvoiceId
WHERE IL.UnitPrice > 0.99;

-- 2. Get the InvoiceDate, customer FirstName and LastName, and Total from all invoices.

SELECT I.InvoiceDate, C.FirstName, C.LastName, I.Total
FROM Invoice I
JOIN Customer C ON I.CustomerId = C.CustomerId;

-- 3. Get the customer FirstName and LastName and the support rep's FirstName and LastName from all customers.

SELECT C.FirstName, C.LastName, E.FirstName, E.LastName
FROM Customer C
JOIN Employee E ON C.SupportRepId = E.EmployeeId;


-- Support reps are on the Employee table.


-- 1. Get the album Title and the artist Name from all albums.

SELECT A.Title, AR.Name
FROM Album A
JOIN Artist AR ON A.ArtistId = AR.ArtistId;

    -- 2. Get all PlaylistTrack TrackIds where the playlist Name is Music.
SELECT 
T.TrackId,
Name
FROM PlaylistTrack T
JOIN Playlist PL 
ON PL.PlaylistId = T.PlaylistId
WHERE PL.Name = 'Music';

-- 3. Get all Track Names for PlaylistId 5.
SELECT TRK.Name
FROM Track TRK
JOIN PlaylistTrack PLT ON PLT.TrackId = TRK.TrackId
WHERE PLT.PlaylistId = 5;

-- 4. Get all Track Names and the playlist Name that they're on.

SELECT TRK.name, PL.Name
FROM Track TRK
JOIN PlaylistTrack PLT ON TRK.TrackId = PLT.TrackId
JOIN Playlist PL ON PLT.PlaylistId = PL.PlaylistId;

-- 5. Get all Track Names and Album Titles that are the genre "Alternative".

SELECT TRK.Name, A.title
FROM Track TRK
JOIN Album A ON TRK.AlbumId = A.AlbumId
JOIN Genre GNR ON GNR.GenreId = TRK.GenreId
WHERE GNR.Name = "Alternative";